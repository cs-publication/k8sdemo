kubectl create deployment k8sdemo --image=name.alp/k8sdemo --dry-run -o=yaml > deployment.yaml
echo --- >> deployment.yaml
kubectl create service clusterip k8sdemo --tcp=8181:8181 --dry-run -o=yaml >> deployment.yaml



# Failed to pull image "registry.gitlab.com/cs-publication/k8sdemo/development:latest": rpc error: code = Unknown desc = Error response from daemon:
# Get https://registry.gitlab.com/v2/individualss/k8sdemo/development/manifests/latest: denied: access forbidden

# registry.gitlab.com/cs-publication/k8sdemo/development:latest


KUBE_NAMESPACE=development
CI_REGISTRY=registry.gitlab.com
CI_REGISTRY_USER=******
CI_REGISTRY_PASSWORD=*****
GITLAB_USER_EMAIL=******
kubectl create secret -n "$KUBE_NAMESPACE" \
gitlab-registry \
--docker-server="$CI_REGISTRY" \
--docker-username="$CI_REGISTRY_USER" \
--docker-password="$CI_REGISTRY_PASSWORD" \
--docker-email="$GITLAB_USER_EMAIL" \
-o yaml --dry-run | sed 's/dockercfg/dockerconfigjson/g' | kubectl replace -n "$KUBE_NAMESPACE" --force -f -


kubectl create secret generic regcred    --from-file=.dockerconfigjson="/Users/alphan.arslan/.docker/.docker/config.json"     --type=kubernetes.io/dockerconfigjson



kubectl expose deployment k8sdemo --type=NodePort
