package name.alp.k8sdemo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/entry-point")
public class EntryPoint {

    @GetMapping
    public ResponseEntity<?> get() {
        String result = "Grand...!";
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
